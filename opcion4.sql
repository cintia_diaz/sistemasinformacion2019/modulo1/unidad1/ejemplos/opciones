﻿-- creando base de datos opcion4
-- es una relacion 1:1 entre ejemplar y socio

DROP DATABASE IF EXISTS opcion4;
CREATE DATABASE opcion4;
USE opcion4;

CREATE TABLE ejemplar(
  cod_ejemplar varchar(10),
  PRIMARY KEY (cod_ejemplar)
  );

CREATE TABLE socio(
  cod_socio varchar(5),
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE presta(
  ejemplar varchar(10),
  socio varchar(5),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (ejemplar, socio),
  UNIQUE KEY (ejemplar),
  UNIQUE KEY (socio),
  CONSTRAINT fkprestaejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT fkprestasocio FOREIGN KEY (socio) REFERENCES socio (cod_socio)
  );

INSERT INTO socio
  VALUES  ('uno'),('dos');

INSERT INTO ejemplar
  VALUES ('quijote'),('titanic');

INSERT INTO presta (ejemplar, socio)
  VALUES ('quijote', 'uno'),
  ('titanic','uno'),('quijote','dos');
       