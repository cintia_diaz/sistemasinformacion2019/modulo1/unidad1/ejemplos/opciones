﻿-- creando base de datos opcion6
-- es una relacion propagada entre ejemplar y socio 1:N

DROP DATABASE IF EXISTS opcion6;
CREATE DATABASE opcion6;
USE opcion6;


CREATE TABLE ejemplar(
  cod_ejemplar varchar(10),
  PRIMARY KEY (cod_ejemplar)
  );

CREATE TABLE socio(
  cod_socio varchar(5),
  ejemplar varchar(10),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (cod_socio),
  CONSTRAINT fksocioejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar)
  );


INSERT INTO ejemplar
  VALUES ('quijote'),('titanic'),('celestina');
INSERT INTO socio  (cod_socio, ejemplar)
  VALUES  ('uno','quijote'),('uno','titanic'),('dos','celestina');


       