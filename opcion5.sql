﻿-- creando base de datos opcion5
-- es una relacion propagada entre ejemplar y socio N:1

DROP DATABASE IF EXISTS opcion5;
CREATE DATABASE opcion5;
USE opcion5;


CREATE TABLE socio(
  cod_socio varchar(5),
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE ejemplar(
  cod_ejemplar varchar(10),
  socio varchar(5),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (cod_ejemplar),
  CONSTRAINT fkejemplarsocio FOREIGN KEY (socio) REFERENCES socio (cod_socio)
  );


INSERT INTO socio
  VALUES  ('uno'),('dos');

INSERT INTO ejemplar (cod_ejemplar, socio)
  VALUES ('quijote','uno'),('titanic','uno'),('celestina','dos');
