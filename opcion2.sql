﻿-- creando base de datos opcion2
-- es una relacion 1:n entre ejemplar y socio

DROP DATABASE IF EXISTS opcion2;
CREATE DATABASE opcion2;
USE opcion2;

CREATE TABLE ejemplar(
  cod_ejemplar varchar(10),
  PRIMARY KEY (cod_ejemplar)
  );

CREATE TABLE socio(
  cod_socio varchar(5),
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE presta(
  ejemplar varchar(10),
  socio varchar(5),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (ejemplar, socio),
  UNIQUE KEY (socio),
  CONSTRAINT fkprestaejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT fkprestasocio FOREIGN KEY (socio) REFERENCES socio (cod_socio)
  );

INSERT INTO ejemplar
  VALUES ('quijote'),('titanic');

INSERT INTO socio
  VALUES  ('uno'),('dos');


INSERT INTO presta (ejemplar, socio)
  VALUES ('quijote', 'uno'),
  ('titanic','uno'),('quijote','dos');
       