﻿-- creando base de datos opcion3
-- es una relacion n:1 entre ejemplar y socio

DROP DATABASE IF EXISTS opcion3;
CREATE DATABASE opcion3;
USE opcion3;

CREATE TABLE ejemplar(
  cod_ejemplar varchar(10),
  PRIMARY KEY (cod_ejemplar)
  );

CREATE TABLE socio(
  cod_socio varchar(5),
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE presta(
  ejemplar varchar(10),
  socio varchar(5),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (ejemplar, socio),
  UNIQUE KEY (ejemplar),
  CONSTRAINT fkprestaejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT fkprestasocio FOREIGN KEY (socio) REFERENCES socio (cod_socio)
  );

INSERT INTO socio
  VALUES  ('uno'),('dos');

INSERT INTO ejemplar
  VALUES ('quijote'),('titanic');

INSERT INTO presta (ejemplar, socio)
  VALUES ('quijote', 'uno'),
  ('titanic','uno'),('quijote','dos');
       