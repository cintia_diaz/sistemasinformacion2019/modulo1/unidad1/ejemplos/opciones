﻿-- creando base de datos opcion1
-- es una relacion n:n entre ejemplar y socio

DROP DATABASE IF EXISTS opcion1;
CREATE DATABASE opcion1;
USE opcion1;

-- create or replace table ejemplar --> para actualizar la tabla si existe o crearla si no
CREATE TABLE ejemplar(
  cod_ejemplar varchar(10),
  PRIMARY KEY (cod_ejemplar)
  );

CREATE TABLE socio(
  cod_socio varchar(5),
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE presta(
  ejemplar varchar(10),
  socio varchar(5),
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (ejemplar, socio),
  CONSTRAINT fkprestaejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT fkprestasocio FOREIGN KEY (socio) REFERENCES socio (cod_socio)
  );

INSERT INTO socio
  VALUES  ('uno'),('dos');

INSERT INTO ejemplar
  VALUES ('quijote'),('titanic');

INSERT INTO presta (ejemplar, socio)
  VALUES ('quijote', 'uno'),
  ('titanic','uno'),('quijote','dos');
       